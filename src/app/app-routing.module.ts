import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AproposComponent } from './apropos/apropos.component';
import { ContactComponent } from './contact/contact.component';
import { FourToFourComponent } from './four-to-four/four-to-four.component';
import { HomeComponent } from './home/home.component';
import { PersonneAjouterComponent } from './personne-ajouter/personne-ajouter.component';
import { PersonneListerComponent } from './personne-lister/personne-lister.component';

// Création de mon routing vers mes components
// To do error 404 - page not found et protection des mauvaises saisies

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: "ajouter-personne", component: PersonneAjouterComponent },
  { path: "apropos", component: AproposComponent },
  { path: "contact", component: ContactComponent },
  { path: "lister-personne", component: PersonneListerComponent },
  { path: "not-found", component: FourToFourComponent },
  { path: "**", redirectTo: 'not-found'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
