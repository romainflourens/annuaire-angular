import { Personne } from "./Models/personne";

// Faire le mock-up qui permet d'injecter les données de test
export let mockPersonnes: Personne[] = [
    { nom: 'Paul', prenom: 'Ochon', tel: '1223' },
    { nom: 'FLOUFLOU', prenom: 'Roro', tel: '1223' },
    { nom: 'Truc', prenom: 'Muche', tel: '1223' }

]; 
