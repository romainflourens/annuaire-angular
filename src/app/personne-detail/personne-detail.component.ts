import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Personne } from '../Models/personne';
import { AnnuaireService } from '../services/annuaire.service';

@Component({
  selector: 'app-personne-detail',
  templateUrl: './personne-detail.component.html',
  styleUrls: ['./personne-detail.component.css']
})
export class PersonneDetailComponent implements OnInit {
  /**
   * Mise en en place des décoteurs @Input() @Output() pour faire appel aux propriétés dans le parent
   */
  // le ! signale que la variable sera initialisée à l'exécution
  @Input() pers !: Personne;
  @Input() index !: number;

  /* permet de rajouter un évènement qui va nous permettre de dire dans le parent 
  * qui ont a supprimer
  */
  @Output() suppressionPersonne = new EventEmitter<Personne>();


  constructor() { }

  ngOnInit(): void {
  }

  /*
    * Méthode permettant de supprimer la personne & surtout de
   * Gèrer le clic sur le bouton'Supprimer' avec le event emitter
 */
  supprimePersonne(personne: Personne) {
    this.suppressionPersonne.emit(personne);
  }
}
//On pourrait faire la même chose sans la fonctionalité EventEmitter mais cela n'a pas le retour escompté car la mise à jour n'est pas faite au clic.

/**
 * constructor(private as: AnnuaireService) { }
*  ngOnInit(): void {
}

    // Gère le clic sur le bouton'Supprimer'

supprimePersonne(personne: Personne) {

  this.as.deletePersonne(personne);
}
}
  */
