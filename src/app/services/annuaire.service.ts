import { Injectable } from '@angular/core';
import { mockPersonnes } from '../mock.personnes';
import { Personne } from '../Models/personne';

@Injectable({
  providedIn: 'root'
})
export class AnnuaireService {
  // Propriété contenant la liste des personnes de l'annuaire
   private personnes: Personne[] = mockPersonnes; 
// NB: C'est ici que j'initialise le tableau des personnes 'Listing Personnes'
     /* Soit par JSON
  []
    { nom: "flourens", tel:"568", prenom: "" },
    { nom: "Test2", prenom: "Toto", tel: "456" }
  ]; 
    */

 /**Soit vide
   * private personnes: Personne[] = [];
  **/

  // soit via la mockUpPersonnes.ts - solution retenue pour notre test 
  
   
  constructor() { }

  //Ajoute une personne à l'annuaire
  ajouter(personne: Personne): void {
    this.personnes.push(personne);
    console.log(this.personnes);
  }

  // Retourne la liste des personnes sous forme de tableau
  getPersonnes(): Personne[] {
    return this.personnes;
  }

  /**
   * Supprime la personne de l'annuaire
   * @param personne la personne à supprimer
     */
  deletePersonne(personne: Personne) {
    this.personnes = this.personnes.filter(p => personne !== p)
  }

}
