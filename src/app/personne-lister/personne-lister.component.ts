import { Component, OnInit } from '@angular/core';
import { Personne } from '../Models/personne';
import { AnnuaireService } from '../services/annuaire.service';

@Component({
  selector: 'app-personne-lister',
  templateUrl: './personne-lister.component.html',
  styleUrls: ['./personne-lister.component.css']
})
export class PersonneListerComponent implements OnInit {

  // définition d'une propriété personnes 
  personnes: any[] = []; 

  constructor(private as: AnnuaireService) { }

  ngOnInit(): void {
    this.majPersonnes();
  }
// Methode permettant de mettre à jour la liste des personnes [] grâce à la méthode issue du 'service annuaire' qui récupère les personnes []
  majPersonnes() {
    this.personnes = this.as.getPersonnes();
  }
// Méthode permettant de faire appel à la méthode deletePersonne du 'service annuaire' qui retourne les éléments filtrées dans la fonction call
// et mets à jour la liste des personnes 
  onSuppressionPersonne (personne: Personne){
    this.as.deletePersonne(personne); 
    this.majPersonnes(); 
  }

}
