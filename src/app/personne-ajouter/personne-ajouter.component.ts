import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Personne } from '../Models/personne';
import { AnnuaireService } from '../services/annuaire.service';

@Component({
  selector: 'app-personne-ajouter',
  templateUrl: './personne-ajouter.component.html',
  styleUrls: ['./personne-ajouter.component.css']
})
export class PersonneAjouterComponent implements OnInit {

  constructor(private as: AnnuaireService) { }

  ngOnInit(): void {
  }
// Methode pour ajouter le formulaire correspondant à une personne via la directive NgForm. 
// Se charge d'ajouter la Form dans l'annuaire Service et de mettre à vide la form
  ajouterPersonne(form: NgForm) {
    // form.value ressemble à {nom : , prenom: , tel:} _ défini par les ngModel name="" dans la vue.
    console.log(form.value);
    this.as.ajouter(this.formValueToPersonne(form.value));
    form.reset();
  }
// boonne pratique à adopter pour eviter d'inscrire directement form.value dans la précedente methode (ajouter())
  formValueToPersonne(formValue: any): Personne {
    return formValue as Personne;
  }


}
